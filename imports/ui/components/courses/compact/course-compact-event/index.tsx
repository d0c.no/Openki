import React from "react";

import { useDateTimeFormat } from "/imports/utils/react-moment-format";

import { EventModel } from "/imports/api/events/events";

import "./styles.scss";

export function CourseCompactEvent(props: { event: EventModel }) {
  const { event } = props;
  const { dateShort, timeFormat, dateToRelativeString } = useDateTimeFormat();

  return (
    <div className="course-compact-next-event col">
      {event.venue?.name ? (
        <div className="course-compact-next-event-component">
          <i className="fa-solid fa-house fa-fw" aria-hidden="true"></i>&nbsp;{event.venue.name}
        </div>
      ) : null}
      <div className="course-compact-next-event-component">
        <i className="fa-regular fa-clock fa-fw" aria-hidden="true"></i>&nbsp;
        <span title={`${dateShort(event?.startLocal)} ${timeFormat(event?.startLocal)}`}>
          {dateToRelativeString(event?.startLocal)}
        </span>
      </div>
      {!event.noRsvp ? (
        <div className="course-compact-next-event-component">
          <i className="fa-solid fa-user fa-fw" aria-hidden="true"></i>&nbsp;
          {event?.numberOfParticipants()}
        </div>
      ) : null}
    </div>
  );
}
